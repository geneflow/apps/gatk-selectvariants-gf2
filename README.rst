GATK SelectVariants GeneFlow App
================================

Version: 4.1.4.1-02

This GeneFlow app wraps the GATK SelectVariants tool.

Inputs
------

1. input: VCF file.

2. reference_sequence: Directory that contains reference sequence fasta, index, and dict files.

Parameters
----------

1. select_type_to_include: Only include this variant type in VCF file: (INDEL, SNP, MIXED, MNP, SYMBOLIC, NO_VARIATION). Default: blank.

2. output: Output directory - The name of the output directory to place VCF and VCF.idx files. Default: output.

